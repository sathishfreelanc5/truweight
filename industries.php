<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>TRUWEIGH</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Stylesheets -->
<!-- <link rel="shortcut icon" type="image/png" href="images/favicon.png" /> -->
<link href="css/bootstrap.css" rel="stylesheet">
<!-- Font Icon -->
<link href="css/stroke-gap-icons.css" rel="stylesheet">
<link href="css/flaticon.css" rel="stylesheet">
<link href="css/font-awesome.min.css" rel="stylesheet">
<link href="css/icofont.css" rel="stylesheet">
<!-- Fancybox -->
<link href="css/jquery.fancybox.css" rel="stylesheet">
<!-- Revolution Slider -->
<link href="css/revolution-slider.css" rel="stylesheet">
<!-- Owl Carousel -->
<link href="css/owl.carousel.css" rel="stylesheet">
<!-- Main CSS -->
<link href="main-style.css" rel="stylesheet">
<!-- Responsive -->
<link href="css/responsive.css" rel="stylesheet">
<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--[if lt IE 9]><script src="js/respond.js"></script><![endif]-->
</head>
<style>
  .yellow-1
  {
    color:#FBCA00;
  }
  .featured-services--1
  {
    background:url("images/slider/industries.png");
    background-size:cover;
    background-position:center;
    background-repeat:no-repeat;
    width:auto;
    height:30vh;
  }
  .featured-services
  {
    background:linear-gradient(170deg,#787474b8,transparent),url(images/pitless_type.jpg);
    background-size:cover;
    background-position:center;
    background-repeat:no-repeat;
    background-attachment: fixed;
  }
  #text--2
  {
    position: relative;
    top:60px;
    left:50px;
  }
  #text--1
  {
    font-size: 60px;
    text-shadow: 3px 3px #e7ca1d;
  }
  .fa-star-half
  {
    margin-right: 10px;
    font-size: 20px;
    color: #8b0000;
  }
  #start-1
  {
    position: absolute;
    left: -300px;
  }
</style>
<body>
<div class="page-wrapper"> 
  <?php include_once 'header.php'; ?>
  <section class="featured-services--1">
      <div class="container">
          <div class="row">
            <div id="text--2">
              <h1 class="text-white" id="text--1" data-aos="fade-left" data-aos-easing="linear" data-aos-duration="1000" data-aos-once="true">Industries</h1>
            </div>
          </div>
        </div>
  </section>
  <!--About Us TrueWay !-->
    <section class="testimonial style_2">
    <div class="container" style='padding-top:30px;padding-bottom:30px;'>
    <div class="section-content" >
        <div class="row">
          <div class="col-md-12" style='padding-top:0px;padding-bottom:20px;'>
            <h2 class=" text-center" data-aos="fade-up" data-aos-easing="linear" data-aos-duration="1000" data-aos-once="true">WEIGHBRIDGES & TRUCK SCALES APPLICATION AREAS</h2> 
          </div>
        </div>
      </div>
      <div class="section-content" id="section-content">
          <div class="row">
            <div class="col-md-6">
              <div class="testimonial_style_2">
                <div class="item">
                  <div class="testimonial-item style_2">
                       <p><i class="fa fa-star-half" aria-hidden="true"></i> Quarries, Mines & Crusher industries</p>
                      <p><i class="fa fa-star-half" aria-hidden="true"></i>Ready Mix Concrete(RMC), Tar plants</p>
                      <p><i class="fa fa-star-half" aria-hidden="true"></i>Poultry farms & Feed mills</p>
                      <p><i class="fa fa-star-half" aria-hidden="true"></i>Coal fields</p>
                      <p><i class="fa fa-star-half" aria-hidden="true"></i>Sea Ports</p>
                      <p><i class="fa fa-star-half" aria-hidden="true"></i>Refineries</p>
                      <p><i class="fa fa-star-half" aria-hidden="true"></i>Export Houses</p>
                      <p><i class="fa fa-star-half" aria-hidden="true"></i>Fertilizers, Chemical & Pharmaceutical Industries</p>
                      <p><i class="fa fa-star-half" aria-hidden="true"></i>Oil Industries</p>
                      <p><i class="fa fa-star-half" aria-hidden="true"></i>Public road weighbridges and for any other suitable applications</p>
                      <p><i class="fa fa-star-half" aria-hidden="true"></i>Rice Mills</p>
                      <p><i class="fa fa-star-half" aria-hidden="true"></i>Godowns/Yards</p>
                  </div>
                </div>
                <div class="item">
                    <div class="testimonial-item style_2" id="s-1">
                      <p><i class="fa fa-star-half" aria-hidden="true"></i> Quarries, Mines & Crusher industries</p>
                      <p><i class="fa fa-star-half" aria-hidden="true"></i>Ready Mix Concrete(RMC), Tar plants</p>
                      <p><i class="fa fa-star-half" aria-hidden="true"></i>Poultry farms & Feed mills</p>
                      <p><i class="fa fa-star-half" aria-hidden="true"></i>Coal fields</p>
                      <p><i class="fa fa-star-half" aria-hidden="true"></i>Sea Ports</p>
                      <p><i class="fa fa-star-half" aria-hidden="true"></i>Refineries</p>
                      <p><i class="fa fa-star-half" aria-hidden="true"></i>Export Houses</p>
                      <p><i class="fa fa-star-half" aria-hidden="true"></i>Fertilizers, Chemical & Pharmaceutical Industries</p>
                      <p><i class="fa fa-star-half" aria-hidden="true"></i>Oil Industries</p>
                      <p><i class="fa fa-star-half" aria-hidden="true"></i>Public road weighbridges and for any other suitable applications</p>
                      <p><i class="fa fa-star-half" aria-hidden="true"></i>Rice Mills</p>
                      <p><i class="fa fa-star-half" aria-hidden="true"></i>Godowns/Yards</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="testimonial_style_2">
              <div class="item">
                  <div class="testimonial-item style_2 ">
                      <p><i class="fa fa-star-half" aria-hidden="true"></i>Steel Industries ( Alloys / Ingot Mfg / TMT / Plate Manufacturing Plants)</p>
                      <p><i class="fa fa-star-half" aria-hidden="true"></i> Cement Industries</p>
                      <p><i class="fa fa-star-half" aria-hidden="true"></i>  Sugar Industries</p>
                      <p><i class="fa fa-star-half" aria-hidden="true"></i>Textile Industries (Spinning / Ginning / Sizing / Dying Mills)</p>
                      <p><i class="fa fa-star-half" aria-hidden="true"></i> Foundry & Engineering Industries</p>
                      <p><i class="fa fa-star-half" aria-hidden="true"></i>Paper Mills</p>
                      <p><i class="fa fa-star-half" aria-hidden="true"></i>  Coconut/Coir Industries</p>
                      <p><i class="fa fa-star-half" aria-hidden="true"></i> Tea Industries</p>
                      <p><i class="fa fa-star-half" aria-hidden="true"></i>Food Industries</p>
                      <p><i class="fa fa-star-half" aria-hidden="true"></i> Ware houses</p>
                      <p><i class="fa fa-star-half" aria-hidden="true"></i> Cold Storage</p>
                      <p><i class="fa fa-star-half" aria-hidden="true"></i>Chemical plants</p>
                  </div>
                </div>
                <div class="item">
                  <div class="testimonial-item style_2 " id="s-1">
                      <p><i class="fa fa-star-half" aria-hidden="true"></i>Steel Industries ( Alloys / Ingot Mfg / TMT / Plate Manufacturing Plants)</p>
                      <p><i class="fa fa-star-half" aria-hidden="true"></i> Cement Industries</p>
                      <p><i class="fa fa-star-half" aria-hidden="true"></i>  Sugar Industries</p>
                      <p><i class="fa fa-star-half" aria-hidden="true"></i>Textile Industries (Spinning / Ginning / Sizing / Dying Mills)</p>
                      <p><i class="fa fa-star-half" aria-hidden="true"></i> Foundry & Engineering Industries</p>
                      <p><i class="fa fa-star-half" aria-hidden="true"></i>Paper Mills</p>
                      <p><i class="fa fa-star-half" aria-hidden="true"></i>  Coconut/Coir Industries</p>
                      <p><i class="fa fa-star-half" aria-hidden="true"></i> Tea Industries</p>
                      <p><i class="fa fa-star-half" aria-hidden="true"></i>Food Industries</p>
                      <p><i class="fa fa-star-half" aria-hidden="true"></i> Ware houses</p>
                      <p><i class="fa fa-star-half" aria-hidden="true"></i> Cold Storage</p>
                      <p><i class="fa fa-star-half" aria-hidden="true"></i>Chemical plants</p>
                  </div>
                </div>
              </div>
              <div id="start-1">

              </div>
            </div>
          </div>
      </div>
      </div>
    </div>
  </section>
<?php include_once 'product-slider.php'; ?>
<?php include_once 'footer.php'; ?>
</div>
<!--End pagewrapper--> 

<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target=".main-header"><span class="icofont icofont-long-arrow-up"></span></div>
<script src="js/jquery.js"></script> 
<script src="js/jquery-ui-1.11.4/jquery-ui.js"></script> 
<script src="js/revolution.min.js"></script> 
<script src="js/rev-custom.js"></script>
<script src="js/all-jquery.js"></script> 
<script src="js/script.js"></script>
<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
<script>
  AOS.init();
</script>
</body>
</html>