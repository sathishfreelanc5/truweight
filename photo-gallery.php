<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>TRUWEIGH</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Stylesheets -->
<!-- <link rel="shortcut icon" type="image/png" href="images/favicon.png" /> -->
<link href="css/bootstrap.css" rel="stylesheet">
<!-- Font Icon -->
<link href="css/stroke-gap-icons.css" rel="stylesheet">
<link href="css/flaticon.css" rel="stylesheet">
<link href="css/font-awesome.min.css" rel="stylesheet">
<link href="css/icofont.css" rel="stylesheet">
<!-- Fancybox -->
<link href="css/jquery.fancybox.css" rel="stylesheet">
<!-- Revolution Slider -->
<link href="css/revolution-slider.css" rel="stylesheet">
<!-- Owl Carousel -->
<link href="css/owl.carousel.css" rel="stylesheet">
<!-- Main CSS -->
<link href="main-style.css" rel="stylesheet">
<!-- Responsive -->
<link href="css/responsive.css" rel="stylesheet">
<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--[if lt IE 9]><script src="js/respond.js"></script><![endif]-->
</head>
<style>
  .yellow-1
  {
    color:#FBCA00;
  }
  .featured-services--1
  {
    background:url("images/slider/gallery.png");
    background-size:cover;
    background-position:center;
    background-repeat:no-repeat;
    width:auto;
    height:30vh;
  }
  .featured-services
  {
    background:linear-gradient(170deg,#787474b8,transparent),url(images/pitless_type.jpg);
    background-size:cover;
    background-position:center;
    background-repeat:no-repeat;
    background-attachment: fixed;
  }
  #text--2
  {
    position: relative;
    top:60px;
    left:50px;
  }
  #text--1
  {
    font-size: 60px;
    text-shadow: 3px 3px #e7ca1d;
  }
</style>
<body>
<div class="page-wrapper"> 
  <?php include_once 'header.php'; ?>
  <section class="featured-services--1">
      <div class="container">
          <div class="row">
            <div id="text--2">
              <h1 class="text-white" id="text--1" data-aos="fade-left" data-aos-easing="linear" data-aos-duration="1000" data-aos-once="true">Photo Gallery</h1>
            </div>
          </div>
        </div>
  </section>
  <!--About Us TrueWay !-->
    <section class="featured-services">
      <div class="container" style='padding-top:30px;padding-bottom:0px;'>
      <div class="section-content">
        <div class="row">
          <div class="col-md-12" style='padding-top:0px;padding-bottom:14px;'>
            <h2 class=" text-theme-color text-center" >Gallery</h2> 
          </div>
        </div>
      </div>
        <div class="section-title">
          <div class="row">
            <div class="col-md-12">
                  <div class="text-center"> 
                    <p style='color:white;'>Truck Weigh Systems India Pvt Ltd being a client centric organization understands the critical role played by efficient after sales services and thus have deployed a field service team that comprises of software and hardware engineers. Structural experts are also available who have the ability to provide immediate support if any mechanical breakdown occurs. Further, we also maintain a sufficient stock of spares which can be supplied by us even on the shortest notice.</p>
                    <p style='color:white;'>Our service department is connected with a dedicated CUSTOMER CARE number which is linked to the Complaint Registration Software ensures the registration of complaints by the customers and getting it solved immediately. We are equipped with sufficient quantity of standard test weights for field calibration of Weighbridges/Truck scales. Every field engineers are equipped with high precision test equipments for quick finding of the problems and resolving it. Our centralized repair center is also equipped with hi-tech trouble shooting instruments and three months forecasted spares.</p>
                 </div>
            </div>
          </div>
        </div>
    </section>

  <!--Section:: Testimonial-->
  <section class="testimonial style_2">
    <div class="container" style='padding-top:50px;padding-bottom:50px;'>
      <div class="section-content">
        <div class="row">
          <div class='col-md-12'>
              <div class='text-center' style='padding-top:10px;padding-bottom:20px;'>
                    <h2>ONSITE / MOBILE CALIBRATION </h2>
              </div>
          </div>
          <div class="col-md-12">
            <div class="testimonial_style_2">
              <div class="item">
                <div class="testimonial-item style_2">
                  <p align="justify"><i class="fa fa-star-half" aria-hidden="true"></i>Truck Weigh System India Pvt Ltd., one of the manufacturing, Supplier, Exporter and Service Providers of Electronic Weighbridges ranging from 5 tonnes to 200 tonnes since 2004.Truck Weigh Systems has launched "Mobile Testing and Calibration(MTaC)" a new and integrated equipment to calibrate the Weighbridges ON SITE. This is ensure 100% accuracy on each weighment to our valued customers.</p>
                </div>
              </div>
              <div class="item">
                <div class="testimonial-item style_2">
                    <p align="justify"><i class="fa fa-star-half" aria-hidden="true"></i>Truck Weigh System India Pvt Ltd., one of the manufacturing, Supplier, Exporter and Service Providers of Electronic Weighbridges ranging from 5 tonnes to 200 tonnes since 2004.Truck Weigh Systems has launched "Mobile Testing and Calibration(MTaC)" a new and integrated equipment to calibrate the Weighbridges ON SITE. This is ensure 100% accuracy on each weighment to our valued customers.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        </div>
      </div>
    </div>
  </section>
  <!--Section:: Testimonial End -->

  <section class="call-to-action bg-black-pearl">
    <div class="container"  style='padding-top:20px;padding-bottom:20px;'>
      <div class="section-content">
        <div class="row">
          <div class="col-md-12">
            <h2 class='text-theme-color text-center'>OUR PRODUCTS</h2> 
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="our_offer bg-light-grey">
    <div class="container pb30" style='padding-top:50px;padding-bottom:50px;'>
        <div class="section-content">
          <div class="row">
            <div class="col-md-3 pn">
              <div class="item">
                  <div class="about-item inner-box">
                      <div class="image">
                          <img src="images/service/ser_1.jpg" alt="" class="img-responsive">
                          <span class="icon flaticon-barrel"></span>

                      </div>
                      <h3 class="mbn"><a href="#">Pit Type Weighbridge</a></h3>
                      <div class="view_more">
                        <a class="ptn" href="#">View details <span class="icofont icofont-arrow-right"></span></a>
                      </div>
                  </div>
              </div>
            </div>
            <div class="col-md-3 pn">
              <div class="item">
                  <div class="about-item inner-box">
                      <div class="image">
                          <img src="images/service/ser_2.jpg" alt="" class="img-responsive">
                          <span class="icon flaticon-harbor-crane"></span>
                          
                      </div>
                      <h3 class="mbn"><a href="#">Pitless Type Weighbridge</a></h3>
                      <div class="view_more">
                        <a class="ptn" href="#">View details <span class="icofont icofont-arrow-right"></span></a>
                      </div>
                  </div>
              </div>
            </div>
            <div class="col-md-3 pn">
              <div class="item">
                  <div class="about-item inner-box">
                      <div class="image">
                          <img src="images/service/ser_3.jpg" alt="" class="img-responsive">
                          <span class="icon flaticon-oil-platform"></span>
                          
                      </div>
                      <h3 class="mbn"><a href="#">Coil Weighbridge</a></h3>
                      <div class="view_more">
                        <a class="ptn" href="#">View details <span class="icofont icofont-arrow-right"></span></a>
                      </div>
                  </div>
              </div>
            </div>
            <div class="col-md-3 pn">
              <div class="item">
                  <div class="about-item inner-box">
                      <div class="image">
                          <img src="images/service/ser_4.jpg" alt="" class="img-responsive">
                          <span class="icon flaticon-tank-1"></span>
                      </div>
                      <h3 class="mbn"><a href="#">Mobile / Portable Weighbridge</a></h3>
                      <div class="view_more">
                        <a class="ptn" href="#">View details <span class="icofont icofont-arrow-right"></span></a>
                      </div>
                  </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-3 pn">
              <div class="item">
                  <div class="about-item inner-box">
                      <div class="image">
                          <img src="images/service/ser_5.jpg" alt="" class="img-responsive">
                          <span class="icon flaticon-barrel"></span>

                      </div>
                      <h3 class="mbn"><a href="#">CONCRETE WEIGHBRIDGE</a></h3>
                      <div class="view_more">
                        <a class="ptn" href="#">View details <span class="icofont icofont-arrow-right"></span></a>
                      </div>
                  </div>
              </div>
            </div>
            <div class="col-md-3 pn">
              <div class="item">
                  <div class="about-item inner-box">
                      <div class="image">
                          <img src="images/service/ser_6.jpg" alt="" class="img-responsive">
                          <span class="icon flaticon-harbor-crane"></span>
                          
                      </div>
                      <h3 class="mbn"><a href="#">Multi Deck</a></h3>
                      <div class="view_more">
                        <a class="ptn" href="#">View details <span class="icofont icofont-arrow-right"></span></a>
                      </div>
                  </div>
              </div>
            </div>
            <div class="col-md-3 pn">
              <div class="item">
                  <div class="about-item inner-box">
                      <div class="image">
                          <img src="images/service/ser_7.jpg" alt="" class="img-responsive">
                          <span class="icon flaticon-oil-platform"></span>
                          
                      </div>
                      <h3 class="mbn"><a href="#">Analog Load Cells</a></h3>
                      <div class="view_more">
                        <a class="ptn" href="#">View details <span class="icofont icofont-arrow-right"></span></a>
                      </div>
                  </div>
              </div>
            </div>
            <div class="col-md-3 pn">
              <div class="item">
                  <div class="about-item inner-box">
                      <div class="image">
                          <img src="images/service/ser_8.jpg" alt="" class="img-responsive">
                          <span class="icon flaticon-tank-1"></span>
                      </div>
                      <h3 class="mbn"><a href="#">Digital Load Cells</a></h3>
                      <div class="view_more">
                        <a class="ptn" href="#">View details <span class="icofont icofont-arrow-right"></span></a>
                      </div>
                  </div>
              </div>
            </div>
          </div>
       </div>
    </div>
  </section> 
<?php include_once 'footer.php'; ?>
</div>
<!--End pagewrapper--> 

<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target=".main-header"><span class="icofont icofont-long-arrow-up"></span></div>
<script src="js/jquery.js"></script> 
<script src="js/jquery-ui-1.11.4/jquery-ui.js"></script> 
<script src="js/revolution.min.js"></script> 
<script src="js/rev-custom.js"></script>
<script src="js/all-jquery.js"></script> 
<script src="js/script.js"></script>
<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
<script>
  AOS.init();
</script>
</body>
</html>