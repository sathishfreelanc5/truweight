<!DOCTYPE html>
<html>


<head>
<meta charset="utf-8">
<title>TRUWEIGH</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Stylesheets -->
<!-- <link rel="shortcut icon" type="image/png" href="images/favicon.png" /> -->
<link href="css/bootstrap.css" rel="stylesheet">
<!-- Font Icon -->
<link href="css/stroke-gap-icons.css" rel="stylesheet">
<link href="css/flaticon.css" rel="stylesheet">
<link href="css/font-awesome.min.css" rel="stylesheet">
<link href="css/icofont.css" rel="stylesheet">
<!-- Fancybox -->
<link href="css/jquery.fancybox.css" rel="stylesheet">
<!-- Revolution Slider -->
<link href="css/revolution-slider.css" rel="stylesheet">
<!-- Owl Carousel -->
<link href="css/owl.carousel.css" rel="stylesheet">
<!-- Main CSS -->
<link href="main-style.css" rel="stylesheet">
<!-- Responsive -->
<link href="css/responsive.css" rel="stylesheet">
<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--[if lt IE 9]><script src="js/respond.js"></script><![endif]-->
</head>
<style>
  /* .yellow-1
  {
    color:#FBCA00;
  } */
  
</style>
<body>
<div class="page-wrapper"> 
  <?php include_once 'header.php'; ?>
  <!-- Slider::Section Start-->
  <?php include_once 'slider.php'; ?>
  <!-- Slider::Section End--> 

  <!--About Us TrueWay !-->
    <section class="featured-services">
      <div class="container" style='padding-top:100px;padding-bottom:0px;'>
        <div class="section-title">
          <div class="row">
            <div class='col-md-2'>&nbsp;</div>
            <div class="col-md-8">
              <div class="title-inner">
                  <div class="text"> 
                    <h2 class=" mb30 text-center brown-color-theme">WELCOME TO  <span>TRUWEIGH</span> </h2>  
                    <div class="sup-1">
                      <h2 style="font-size:20px;padding-bottom:5px;" class="text-center">Suppliers & Service Providers of all Types of Electronic Weighbridges & Weighing Automation Systems</h2>
                    </div>
                    <p class='text-center'>TRUWEIGH® is a well established Brand in the field of Electronic Weighbridges under the Company named “TRUWEIGH SYSTEMS INDIA PVT. LTD.” who is one of the leading Manufacturers, Suppliers, Service Providers & Calibrators of all types of Electronic Weighbridges, Weighing Systems and Weighing Automation Solutions. With the vast experience in the field of Weighing for more than 50 years</p>
                 </div>
              </div>
            </div>
            <div class='col-md-2'>&nbsp;</div>
          </div>
        </div>
    </section>

  <!-- Section:: Call To Action -->
  <section class="call-to-action bg-black-pearl">
    <div class="container"  style='padding-top:50px;padding-bottom:50px;'>
      <div class="section-content">
        <div class="row">
          <div class="col-md-10">
            <h6>Looking for a quality</h6>
            <h2 class="small-line text-uppercase mb30 text-white">STRUCTURE
            <span class="text-theme-color"> <br>DESIGN ANALYSIS</span></h2> 
          </div>
          <div class="col-md-2 mt30">
            <a href="#" class="btn theme-second-btn mt20">Our Responsibility</a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- Section:: Call To Action End -->

  <!--Section:: Testimonial-->
  <section class="testimonial style_2">
    <div class="container" style='padding-top:50px;padding-bottom:50px;'>
      <div class="section-content">
        <div class="row">
          <div class='col-md-12'>
              <div class='text-center' style='padding-top:10px;padding-bottom:20px;'>
                    <h2>TESTIMONIAL</h2>
              </div>
          </div>
          <div class="col-md-4">
            <div class="testimonial_style_2">
              <div class="item">
                <div class="testimonial-item style_2">
                 <blockquote> 
                  <!-- <div class="testimonial-thumb pb20">
                    <img class="img-circle" src="images/testimonial/4.jpg" alt="">
                  </div> -->
                        <p>All types of fully electronic weigh bridges & systems with Flintec Digital load cell which adopts the latest cutting edge technology.  All types of fully electronic weigh bridges & systems with Flintec Digital load cell which adopts the latest cutting edge technology.</p>
                  </blockquote>
                </div>
              </div>
              <div class="item">
                <div class="testimonial-item style_2">
                 <blockquote> 
                  <!-- <div class="testimonial-thumb pb20">
                    <img class="img-circle" src="images/testimonial/4.jpg" alt="">
                  </div> -->
                        <p>All types of fully electronic weigh bridges & systems with Flintec Digital load cell which adopts the latest cutting edge technology.  All types of fully electronic weigh bridges & systems with Flintec Digital load cell which adopts the latest cutting edge technology.</p>
                  </blockquote>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-8">
            <div class="opsition">
              <div class="rich-text">
                <h2>Let's Grow<br>
                  <strong>Together</strong>
                </h2>
                <p class="text-white">Join our honor winning group, and appreciate an inventive, <br> dynamic and comprehensive culture concentrated on one objective.</p>
                <a href="contact.html" class="btn theme-btn mt20">See Open careers</a>
              </div>
            </div>
          </div>
        </div>
        </div>
      </div>
    </div>
  </section>
  <!-- <section>
    <div class="container"  style='padding-top:50px;padding-bottom:50px;'>
      <div class="row">
      <div class="col-md-12">
        <div class='text-center'  style='padding-top:10px;padding-bottom:20px;'>
            <h2>
              NEWS & <span class='yellow-1'>EVENTS</span>
            </h2>
        </div>
      </div>
      <div class="col-md-4">
          <div class="clients-slider-1">
          <article class="post item">
                <div class="entry-thumbnail">
                  <img class="img-responsive" src="images/blog/10.jpg" alt="">
                  <div class="post-meta">
                    <span class="pub-date">28, Mar 2022</span>
                  </div>
                </div>
                <div class="post-content">
                  <div class="entry-title">
                    <h3>Mobile Calibration Truck</h3>
                  </div>
                  <div class="entry-content">
                    <p>TRUWEIGH® have installed 2 Nos of 100 Tons Weighbridges in MORMUGAO PORT TRUST on Built, Own, Operate & Maintenance (BOOM) Scheme and now it has successfully completed ONE YEAR OF COMPLETION by doing hundreds of TRUCKS every day with fully Automated Software integrating the DIRECTORATE OF MINING & GEOLOGY of GOA GOVERNMENT to give real time weight data to the Department & to the Port.</p>
                    <div class="readmore">
                      <a href="#"><i class="icofont icofont-curved-right"></i></a>
                    </div>
                  </div>
                </div>
            </article>
            <article class="post item">
                <div class="entry-thumbnail">
                  <img class="img-responsive" src="images/blog/10.jpg" alt="">
                  <div class="post-meta">
                    <span class="pub-date">28, Mar 2022</span>
                  </div>
                </div>
                <div class="post-content">
                  <div class="entry-title">
                    <h3>Mobile Calibration Truck</h3>
                  </div>
                  <div class="entry-content">
                    <p>TRUWEIGH® have installed 2 Nos of 100 Tons Weighbridges in MORMUGAO PORT TRUST on Built, Own, Operate & Maintenance (BOOM) Scheme and now it has successfully completed ONE YEAR OF COMPLETION by doing hundreds of TRUCKS every day with fully Automated Software integrating the DIRECTORATE OF MINING & GEOLOGY of GOA GOVERNMENT to give real time weight data to the Department & to the Port.</p>
                    <div class="readmore">
                      <a href="#"><i class="icofont icofont-curved-right"></i></a>
                    </div>
                  </div>
                </div>
            </article>
          </div>
        </div>
        <div class="col-md-4">
          <div class="clients-slider-1">
          <article class="post item">
                <div class="entry-thumbnail">
                  <img class="img-responsive" src="images/blog/10.jpg" alt="">
                  <div class="post-meta">
                    <span class="pub-date">28, Mar 2022</span>
                  </div>
                </div>
                <div class="post-content">
                  <div class="entry-title">
                    <h3>Mobile Calibration Truck</h3>
                  </div>
                  <div class="entry-content">
                    <p>TRUWEIGH® have installed 2 Nos of 100 Tons Weighbridges in MORMUGAO PORT TRUST on Built, Own, Operate & Maintenance (BOOM) Scheme and now it has successfully completed ONE YEAR OF COMPLETION by doing hundreds of TRUCKS every day with fully Automated Software integrating the DIRECTORATE OF MINING & GEOLOGY of GOA GOVERNMENT to give real time weight data to the Department & to the Port.</p>
                    <div class="readmore">
                      <a href="#"><i class="icofont icofont-curved-right"></i></a>
                    </div>
                  </div>
                </div>
            </article>
            <article class="post item">
                <div class="entry-thumbnail">
                  <img class="img-responsive" src="images/blog/10.jpg" alt="">
                  <div class="post-meta">
                    <span class="pub-date">28, Mar 2022</span>
                  </div>
                </div>
                <div class="post-content">
                  <div class="entry-title">
                    <h3>Mobile Calibration Truck</h3>
                  </div>
                  <div class="entry-content">
                    <p>TRUWEIGH® have installed 2 Nos of 100 Tons Weighbridges in MORMUGAO PORT TRUST on Built, Own, Operate & Maintenance (BOOM) Scheme and now it has successfully completed ONE YEAR OF COMPLETION by doing hundreds of TRUCKS every day with fully Automated Software integrating the DIRECTORATE OF MINING & GEOLOGY of GOA GOVERNMENT to give real time weight data to the Department & to the Port.</p>
                    <div class="readmore">
                      <a href="#"><i class="icofont icofont-curved-right"></i></a>
                    </div>
                  </div>
                </div>
            </article>
            
          </div>
        </div>
        <div class="col-md-4">
          <div class="clients-slider-1">
          <article class="post item">
                <div class="entry-thumbnail">
                  <img class="img-responsive" src="images/blog/10.jpg" alt="">
                  <div class="post-meta">
                    <span class="pub-date">28, Mar 2022</span>
                  </div>
                </div>
                <div class="post-content">
                  <div class="entry-title">
                    <h3>Mobile Calibration Truck</h3>
                  </div>
                  <div class="entry-content">
                    <p>TRUWEIGH® have installed 2 Nos of 100 Tons Weighbridges in MORMUGAO PORT TRUST on Built, Own, Operate & Maintenance (BOOM) Scheme and now it has successfully completed ONE YEAR OF COMPLETION by doing hundreds of TRUCKS every day with fully Automated Software integrating the DIRECTORATE OF MINING & GEOLOGY of GOA GOVERNMENT to give real time weight data to the Department & to the Port.</p>
                    <div class="readmore">
                      <a href="#"><i class="icofont icofont-curved-right"></i></a>
                    </div>
                  </div>
                </div>
            </article>
            <article class="post item">
                <div class="entry-thumbnail">
                  <img class="img-responsive" src="images/blog/10.jpg" alt="">
                  <div class="post-meta">
                    <span class="pub-date">28, Mar 2022</span>
                  </div>
                </div>
                <div class="post-content">
                  <div class="entry-title">
                    <h3>Mobile Calibration Truck</h3>
                  </div>
                  <div class="entry-content">
                    <p>TRUWEIGH® have installed 2 Nos of 100 Tons Weighbridges in MORMUGAO PORT TRUST on Built, Own, Operate & Maintenance (BOOM) Scheme and now it has successfully completed ONE YEAR OF COMPLETION by doing hundreds of TRUCKS every day with fully Automated Software integrating the DIRECTORATE OF MINING & GEOLOGY of GOA GOVERNMENT to give real time weight data to the Department & to the Port.</p>
                    <div class="readmore">
                      <a href="#"><i class="icofont icofont-curved-right"></i></a>
                    </div>
                  </div>
                </div>
            </article>
            
          </div>
        </div>
      </div>
    </div>
  </section>
  -->
<?php include_once 'product-slider.php'; ?>
<?php include_once 'footer.php'; ?>
</div>
<!--End pagewrapper--> 

<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target=".main-header"><span class="icofont icofont-long-arrow-up"></span></div>
<script src="js/jquery.js"></script> 
<script src="js/jquery-ui-1.11.4/jquery-ui.js"></script> 
<script src="js/revolution.min.js"></script> 
<script src="js/rev-custom.js"></script>
<script src="js/all-jquery.js"></script> 
<script src="js/script.js"></script>
</body>


</html>