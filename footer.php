  <!--Section::Footer-->
  <footer class="main-footer">
    <div class="container" style='padding-top:27px;padding-bottom:0px;'>
        <ul id="social_side_links" >
        <li><a  href="#" target="_blank"><img src="images/facebook-icon.png" alt="" ></a></li>
        <li><a  href="#" target="_blank" ><img src="images/linkedin.png" alt="" ></a></li>
        <li><a  href="https://wa.me/+919362277320" target="_blank"><img  src="images/whatsapp-icon.png" alt=""></a></li> 
        </ul>
      <div class="row clearfix">
        <div class="col-md-2">
          <div class="footer-1"> <a href="index.php"><img src="images/logo.jpg" alt="logo" class="logo-footer"></a></div>
        </div>
        <!--Footer Column-->
        <div class="col-md-4">
          <div class="footer-2">
              <h4 style='color:#000000;'>Contact us</h4>
              <p><span style='color:#000000;font-weight:bold;'>Address :</span> <br>
                TRUWEIGH SYSTEMS INDIA PRIVATE LIMITED<br>
                COSMAFAN Foundry Cluster Park - 1,<br>
                Arasur, Coimbatore -641407<br>
                TAMIL NADU, INDIA.<br>
              </p>
          </div>
        </div>
        <!--Footer Column-->
        <div class="col-md-4 ">
          <div class="footer-3">
            <p><span style="color:#000000;">Call Us :</span> <br>
            <i class="fa fa-phone" aria-hidden="true"></i>
              +91 93622 77320
            </p>
            <p><span style="color:#000000;">Email :</span> <br>
            <i class="fa fa-envelope-o" aria-hidden="true"></i>
              sales@truweigh.in
            </p>
          </div>
        </div>
        <!--Footer Column-->
        <div class="col-md-2 ">
          <div class="footer-4">
            <div class="flickr-feed clearfix">
           <ul class="social">
              <li><a href="#"><i class="fa fa-facebook-official" aria-hidden="true" style='font-size:30px;border-raduis:60px;'></i></a></li>
            </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--Copyright-->
   <div class="copyright"><a target="_blank" href="https://www.mediawonderz.com/">@2022 All Rights Reserved | Designed by Media Wonderz</a></div>
  </footer>
 
  <!--Section::Footer End --> 
  