<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!--AOS CSS-->
   <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
<!-- Scroll Reveal-->
<script src="https://unpkg.com/scrollreveal"></script>
  <!-- AOS SCRIPT-->
  <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <!-- Bootstrap4 Css-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  </head>
  <style>
  
/* Slider */
#slider{
    width: 100%;
    height:100%;
    position: relative;
    overflow: hidden;
}
@keyframes load{
  from{left:-100%;}
  to{left:0;}
}
.slides{
  width:400%;
  height:100%;
  position:relative;
  -webkit-animation:slide 30s infinite;
  -moz-animation:slide 30s infinite;
  animation:slide 30s infinite;
}
.slider{
  width:25%;
  height:100%;
  float:left;
  position:relative;
  z-index:1;
  overflow:hidden;
}
.slide img{
  width:100%;
  height:100%;
}
.slide img{
  width:100%;
  height:100%;
}
.image{
  width:100%;
  height:100%;
}
.image img{
  width:100%;
  height:auto;
}

/* Legend */
.legend{
  border:500px solid transparent;
  border-bottom:0;
  position:absolute;
  bottom:0;
}

/* Contents */
.content{
  width:100%;
  height:100%;
  position:absolute;
  overflow:hidden;
}
.content-txt{
  width: 1200px;
    height: 150px;
    float: left;
    position: relative;
    top: 303px;
    left: 9%;
    -webkit-animation: content-s 7.5s infinite;
    -moz-animation: content-s 7.5s infinite;
    animation: content-s 7.5s infinite;
}
.content-txt h1 {
  color: white;
    text-shadow: 1px 1px 1px #000;
    font-family: Arial;
    text-transform: uppercase;
    color: #fff;
    text-align: left;
    font-size: 46px;
    text-align: center;
    /* margin-left: -353
px
; */
    margin-top: 100px;
}
.content-txt h2{
  font-family:arial;
  font-weight:normal;
  font-size:81px;
  font-style:italic;
  color:#fff;
  text-align:left;
  margin-left:30px;
}

/* Switch */
.switch{
  width:120px;
  height:10px;
  position:absolute;
  bottom:50px;
  z-index:99;
  left:30px;
}
.switch > ul{
  list-style:none;
}
.switch > ul > li{
  width:10px;
  height:10px;
  border-radius:50%;
  background:#333;
  float:left;
  margin-right:5px;
  cursor:pointer;
}
.switch ul{
  overflow:hidden;
  display:none;
}
.on{
  width:100%;
  height:100%;
  border-radius:50%;
  background:#f39c12;
  position:relative;
  -webkit-animation:on 30s infinite;
  -moz-animation:on 30s infinite;
  animation:on 30s infinite;
}

/* Animation */
@-webkit-keyframes slide{
  0%,100%{
    margin-left:0%;
  }
  21%{
    margin-left:0%;
  }
  25%{
    margin-left:-100%;
  }
  46%{
    margin-left:-100%;
  }
  50%{
    margin-left:-200%;
  }
  71%{
    margin-left:-200%;
  }
  75%{
    margin-left:0%;
  }
  /* 0%,100%{
    margin-right:0%;
  } */
}
@-moz-keyframes slide{
  0%,100%{
    margin-left:0%;
  }
  21%{
    margin-left:0%;
  }
  25%{
    margin-left:-100%;
  }
  46%{
    margin-left:-100%;
  }
  50%{
    margin-left:-200%;
  }
  71%{
    margin-left:-200%;
  }
  75%{
    margin-left:0%;
  }
  /* 0%,100%{
    margin-right:0%;
  } */
}

@keyframes slide{
  0%,100%{
    margin-left:0%;
  }
  21%{
    margin-left:0%;
  }
  25%{
    margin-left:-100%;
  }
  46%{
    margin-left:-100%;
  }
  50%{
    margin-left:-200%;
  }
  71%{
    margin-left:-200%;
  }
  75%{
    margin-left:0%;
  }
  /* 0%,100%{
    margin-right:0%;
  } */
}



@-webkit-keyframes on{
  0%,100%{
    margin-left:0%;
  }
  21%{
    margin-left:0%;
  }
  25%{
    margin-left:15px;
  }
  46%{
    margin-left:15px;
  }
  50%{
    margin-left:30px;
  }
  71%{
    margin-left:30px;
  }
  75%{
    margin-left:45px;
  }
  0%,100%{
    margin-left:0%;
  }
}

@-moz-keyframes on{
  0%,100%{
    margin-left:0%;
  }
  21%{
    margin-left:0%;
  }
  25%{
    margin-left:15px;
  }
  46%{
    margin-left:15px;
  }
  50%{
    margin-left:30px;
  }
  71%{
    margin-left:30px;
  }
  75%{
    margin-left:45px;
  }
  0%,100%{
    margin-left:0%;
  }
}

@keyframes on{
  0%,100%{
    margin-left:0%;
  }
  21%{
    margin-left:0%;
  }
  25%{
    margin-left:15px;
  }
  46%{
    margin-left:15px;
  }
  50%{
    margin-left:30px;
  }
  71%{
    margin-left:30px;
  }
  75%{
    margin-left:45px;
  }
  0%,100%{
    margin-left:0%;
  }
}

@media only screen and (max-width:767px)
{
    #slider 
    {
        width: 100%;
        height: 100%;
        position: relative;
        top:0px;
        overflow: hidden;
    }
}
  </style>
<body>

<!-- Slider -->
<div id="slider">
  <div class="slides">
    <div class="slider">
      <div class="legend"></div>
      <div class="content">
        <div class="content-txt">
          <!-- <h2
          
                  data-aos="fade-left"
                  data-aos-offset='200'
                  data-aos-delay='50'
                  data-aos-duration="1000" 
                  data-aos-easing="ease-in-out"
                  data-aos-mirror="true"
                  data-aos-once="true"
          >SK INDUSTRIES GAS</h2> -->
        </div>
      </div>
      <div class="image">
        <img src="images/slider/2.jpg">
      </div>
    </div>
    <div class="slider">
      <div class="legend"></div>
      <div class="content">
        <div class="content-txt">
        <!-- <h2
          
          data-aos="fade-left"
          data-aos-offset='200'
          data-aos-delay='50'
          data-aos-duration="1000" 
          data-aos-easing="ease-in-out"
          data-aos-mirror="true"
          data-aos-once="true"
  >SK INDUSTRIES GAS</h2> -->
        </div>
      </div>
      <div class="image">
      <img src="images/slider/3.jpg">
      </div>
    </div>
    <div class="slider">
      <div class="legend"></div>
      <div class="content">
        <div class="content-txt">
          
        <!-- <h2
          
                  data-aos="fade-left"
                  data-aos-offset='200'
                  data-aos-delay='50'
                  data-aos-duration="1000" 
                  data-aos-easing="ease-in-out"
                  data-aos-mirror="true"
                  data-aos-once="true"
          >SK INDUSTRIES GAS</h2> -->
        </div>
      </div>
      <div class="image">
      <img src="images/slider/1.jpg">
      </div>
    </div>
    <div class="slider">
      <div class="legend"></div>
      <div class="content">
        <div class="content-txt">
          
        <!-- <h2
          
                  data-aos="fade-left"
                  data-aos-offset='200'
                  data-aos-delay='50'
                  data-aos-duration="1000" 
                  data-aos-easing="ease-in-out"
                  data-aos-mirror="true"
                  data-aos-once="true"
          >SK INDUSTRIES GAS</h2> -->
        </div>
      </div>
      <div class="image">
      <img src="images/slider/4.jpg">
      </div>
    </div>
    <div class="slider">
      <div class="legend"></div>
      <div class="content">
        <div class="content-txt">
          
        <!-- <h2
          
                  data-aos="fade-left"
                  data-aos-offset='200'
                  data-aos-delay='50'
                  data-aos-duration="1000" 
                  data-aos-easing="ease-in-out"
                  data-aos-mirror="true"
                  data-aos-once="true"
          >SK INDUSTRIES GAS</h2> -->
        </div>
      </div>
      <div class="image">
      <img src="images/slider/.jpg">
      </div>
    </div>
  </div>
  <div class="switch">
    <ul>
      <li>
        <div class="on"></div>
      </li>
      <li></li>
      <li></li>
    </ul>
  </div>
</div>

</body>
</html>