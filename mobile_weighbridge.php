<!DOCTYPE html>
<html>


<head>
<meta charset="utf-8">
<title>TRUWEIGH</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Stylesheets -->
<!-- <link rel="shortcut icon" type="image/png" href="images/favicon.png" /> -->
<link href="css/bootstrap.css" rel="stylesheet">
<!-- Font Icon -->
<link href="css/stroke-gap-icons.css" rel="stylesheet">
<link href="css/flaticon.css" rel="stylesheet">
<link href="css/font-awesome.min.css" rel="stylesheet">
<link href="css/icofont.css" rel="stylesheet">
<!-- Fancybox -->
<link href="css/jquery.fancybox.css" rel="stylesheet">
<!-- Revolution Slider -->
<link href="css/revolution-slider.css" rel="stylesheet">
<!-- Owl Carousel -->
<link href="css/owl.carousel.css" rel="stylesheet">
<!-- Main CSS -->
<link href="main-style.css" rel="stylesheet">
<!-- Responsive -->
<link href="css/responsive.css" rel="stylesheet">
<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--[if lt IE 9]><script src="js/respond.js"></script><![endif]-->
</head>
<style>
  .yellow-1
  {
    color:#FBCA00;
  }
  .featured-services--1
  {
    background:url("images/slider/product-banner.jpg");
    background-size:cover;
    background-position:center;
    background-repeat:no-repeat;
    width:auto;
    height:30vh;
  }
  .featured-services
  {
    background:linear-gradient(170deg,#787474b8,transparent),url(images/pitless_type.jpg);
    background-size:cover;
    background-position:center;
    background-repeat:no-repeat;
    background-attachment: fixed;
  }
  .blue-1
  {
      color:#0d599d;
  }
  .content-1
  {
    background:url("images/slider/content-banner.png");
    background-size:cover;
    background-position:center;
    background-repeat:no-repeat;
    height:25vh;
  }
  
</style>
<body>
<div class="page-wrapper"> 
  <?php include_once 'header.php'; ?>
  <section class="featured-services--1">
      <div class="container">
          <div class="row">

          </div>
        </div>
  </section>
  <section class="featured-services--3">
      <div class="container" style='padding-top:50px;padding-bottom:50px;'>
      <div class="section-content">
        <div class="row">
          <div class="col-md-12" style='padding-top:0px;padding-bottom:14px;'>
            <h2 class="text-center" >MOBILE / PORTABLE  WEIGHBRIDGE </h2> 
          </div>
        </div>
      </div>
        <div class="section-title" style="padding-top: 100px;">
          <div class="row">
            <div class="col-md-6">
                  <div class="text-center"> 
                    <p style="line-height: 40px;padding-top: 50px;">
                      We manufacture comprehensive range of portable / Mobile weighbridges, Portable Weighbridge, with capacities as required by our clients. Our transportable weighbridges are helping to control and monitor of bulk materials across industry, in manufacturing, ports landfill, recycling, road construction and farming application.
                      We supply a complete range of steel and concrete deck transportable weighbridges, Portable Weighbridge, designed for long life and consistent performance and to bring down costs for owner operators. Transportable weighbridges can be pit or surface mounted, with ramps for easier access and each features our high performance load cells for total reliability.
                      Instrumentation and software packages are also provided if required by the clients. Necessary quality tests are conducted to ensure that our manufactured transportable weighbridges are at par with the international standard.
                  </p>
                 </div>
            </div>
            <div class="col-md-6">
            <div class="testimonial_style_2">
              <div class="item">
                <div class="testimonial-item style_2">
                    <img src='images/mobile.jpg' style='width:100%;height:auto;'>
                </div>
              </div>
              <div class="item">
                <div class="testimonial-item style_2">
                    <img src='images/mobile.jpg' style='width:100%;height:auto;'>
                </div>
              </div>
            </div>
          </div>
          </div>
        </div>
    </section>
  <section class="testimonial style_2">
    <div class="container" style='padding-top:30px;padding-bottom:30px;'>
      <div class="section-content">
        <div class="row">
         
        </div>
        </div>
      </div>
    </div>
  </section>
  <section class="content-1">
    <div class="container">
      <div class="section-content">
        <div class="row">
          <div class="col-md-12">
        
          </div>
        </div>
      </div>
    </div>
  </section>
<?php include_once 'product-slider.php'; ?>
<?php include_once 'footer.php'; ?>
</div>
<!--End pagewrapper--> 

<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target=".main-header"><span class="icofont icofont-long-arrow-up"></span></div>
<script src="js/jquery.js"></script> 
<script src="js/jquery-ui-1.11.4/jquery-ui.js"></script> 
<script src="js/revolution.min.js"></script> 
<script src="js/rev-custom.js"></script>
<script src="js/all-jquery.js"></script> 
<script src="js/script.js"></script>
</body>
</html>