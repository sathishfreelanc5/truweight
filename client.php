<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>TRUWEIGH</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Stylesheets -->
<!-- <link rel="shortcut icon" type="image/png" href="images/favicon.png" /> -->
<link href="css/bootstrap.css" rel="stylesheet">
<!-- Font Icon -->
<link href="css/stroke-gap-icons.css" rel="stylesheet">
<link href="css/flaticon.css" rel="stylesheet">
<link href="css/font-awesome.min.css" rel="stylesheet">
<link href="css/icofont.css" rel="stylesheet">
<!-- Fancybox -->
<link href="css/jquery.fancybox.css" rel="stylesheet">
<!-- Revolution Slider -->
<link href="css/revolution-slider.css" rel="stylesheet">
<!-- Owl Carousel -->
<link href="css/owl.carousel.css" rel="stylesheet">
<!-- Main CSS -->
<link href="main-style.css" rel="stylesheet">
<!-- Responsive -->
<link href="css/responsive.css" rel="stylesheet">
<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--[if lt IE 9]><script src="js/respond.js"></script><![endif]-->
</head>
<style>
  .yellow-1
  {
    color:#FBCA00;
  }
  .featured-services--1
  {
    background:url("images/slider/client.png");
    background-size:cover;
    background-position:center;
    background-repeat:no-repeat;
    width:auto;
    height:30vh;
  }
  .featured-services
  {
    background:linear-gradient(170deg,#787474b8,transparent),url(images/pitless_type.jpg);
    background-size:cover;
    background-position:center;
    background-repeat:no-repeat;
    background-attachment: fixed;
  }
  #text--2
  {
    position: relative;
    top:60px;
    left:50px;
  }
  #text--1
  {
    font-size: 60px;
    text-shadow: 3px 3px #e7ca1d;
  }
</style>
<body>
<div class="page-wrapper"> 
  <?php include_once 'header.php'; ?>
  <section class="featured-services--1">
      <div class="container">
          <div class="row">
            <div id="text--2">
              <h1 class="text-white" id="text--1" data-aos="fade-left" data-aos-easing="linear" data-aos-duration="1000" data-aos-once="true">Clients</h1>
            </div>
          </div>
        </div>
  </section>

  <!--Section:: Testimonial-->
  <section class="testimonial style_2">
    <div class="container" style='padding-top:50px;padding-bottom:50px;'>
      <div class="section-content">
        <div class="row">
          <div class='col-md-12'>
              <div class='text-center' style='padding-top:10px;padding-bottom:20px;'>
                    <h2 data-aos="fade-up" data-aos-easing="linear" data-aos-duration="1000" data-aos-once="true">OUR VALUABLE CLIENTS</h2>
              </div>
          </div>
          <div class="col-md-3">
            <div class="testimonial_style_2">
              <div class="item">
                <div class="testimonial-item style_2">
                    <img src="images/client/1.jpg" style="width:100%;height:auto;">
                </div>
              </div>
              <div class="item">
                <div class="testimonial-item style_2">
                  <img src="images/client/2.jpg" style="width:100%;height:auto;">
                </div>
              </div>
              <div class="item">
                <div class="testimonial-item style_2">
                    <img src="images/client/3.jpg" style="width:100%;height:auto;">
                </div>
              </div>
              <div class="item">
                <div class="testimonial-item style_2">
                  <img src="images/client/4.jpg" style="width:100%;height:auto;">
                </div>
              </div>
              <div class="item">
                <div class="testimonial-item style_2">
                  <img src="images/client/5.jpg" style="width:100%;height:auto;">
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="testimonial_style_2">
              <div class="item">
                <div class="testimonial-item style_2">
                  <img src="images/client/6.jpg" style="width:100%;height:auto;">
                </div>
              </div>
              <div class="item">
                <div class="testimonial-item style_2">
                  <img src="images/client/7.jpg" style="width:100%;height:auto;">
                </div>
              </div>
              <div class="item">
                <div class="testimonial-item style_2">
                    <img src="images/client/8.jpg" style="width:100%;height:auto;">
                </div>
              </div>
              <div class="item">
                <div class="testimonial-item style_2">
                  <img src="images/client/9.jpg" style="width:100%;height:auto;">
                </div>
              </div>
              <div class="item">
                <div class="testimonial-item style_2">
                  <img src="images/client/10.jpg" style="width:100%;height:auto;">
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="testimonial_style_2">
              <div class="item">
                <div class="testimonial-item style_2">
                    <img src="images/client/11.jpg" style="width:100%;height:auto;">
                </div>
              </div>
              <div class="item">
                <div class="testimonial-item style_2">
                    <img src="images/client/12.jpg" style="width:100%;height:auto;">
                </div>
              </div>
              <div class="item">
                <div class="testimonial-item style_2">
                  <img src="images/client/13.jpg" style="width:100%;height:auto;">
                </div>
              </div>
              <div class="item">
                <div class="testimonial-item style_2">
                  <img src="images/client/14.jpg" style="width:100%;height:auto;">
                </div>
              </div>
              <div class="item">
                <div class="testimonial-item style_2">
                  <img src="images/client/15.jpg" style="width:100%;height:auto;">
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="testimonial_style_2">
              <div class="item">
                <div class="testimonial-item style_2">
                    <img src="images/client/25.jpg" style="width:100%;height:auto;">
                </div>
              </div>
              <div class="item">
                <div class="testimonial-item style_2">
                    <img src="images/client/26.jpg" style="width:100%;height:auto;">
                </div>
              </div>
              <div class="item">
                <div class="testimonial-item style_2">
                    <img src="images/client/27.jpg" style="width:100%;height:auto;">
                </div>
              </div>
              <div class="item">
                <div class="testimonial-item style_2">
                    <img src="images/client/28.jpg" style="width:100%;height:auto;">
                </div>
              </div>
              <div class="item">
                <div class="testimonial-item style_2">
                    <img src="images/client/29.jpg" style="width:100%;height:auto;">
                </div>
              </div>
              <div class="item">
                <div class="testimonial-item style_2">
                    <img src="images/client/30.jpg" style="width:100%;height:auto;">
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-3" style="padding-top: 30px;">
            <div class="testimonial_style_2">
              <div class="item">
                <div class="testimonial-item style_2">
                    <img src="images/client/16.jpg" style="width:100%;height:auto;">
                </div>
              </div>
              <div class="item">
                <div class="testimonial-item style_2">
                    <img src="images/client/17.jpg" style="width:100%;height:auto;">
                </div>
              </div>
              <div class="item">
                <div class="testimonial-item style_2">
                    <img src="images/client/18.jpg" style="width:100%;height:auto;">
                </div>
              </div>
              <div class="item">
                <div class="testimonial-item style_2">
                    <img src="images/client/19.jpg" style="width:100%;height:auto;">
                </div>
              </div>
              <div class="item">
                <div class="testimonial-item style_2">
                    <img src="images/client/20.jpg" style="width:100%;height:auto;">
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-3" style="padding-top: 30px;">
            <div class="testimonial_style_2">
              <div class="item">
                <div class="testimonial-item style_2">
                    <img src="images/client/21.jpg" style="width:100%;height:auto;">
                </div>
              </div>
              <div class="item">
                <div class="testimonial-item style_2">
                    <img src="images/client/22.jpg" style="width:100%;height:auto;">
                </div>
              </div>
              <div class="item">
                <div class="testimonial-item style_2">
                    <img src="images/client/23.jpg" style="width:100%;height:auto;">
                </div>
              </div>
              <div class="item">
                <div class="testimonial-item style_2">
                    <img src="images/client/24.jpg" style="width:100%;height:auto;">
                </div>
              </div>
              <div class="item">
                <div class="testimonial-item style_2">
                    <img src="images/client/25.jpg" style="width:100%;height:auto;">
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-3" style="padding-top: 30px;">
            <div class="testimonial_style_2">
              <div class="item">
                <div class="testimonial-item style_2">
                    <img src="images/client/5.jpg" style="width:100%;height:auto;">
                </div>
              </div>
              <div class="item">
                <div class="testimonial-item style_2">
                    <img src="images/client/4.jpg" style="width:100%;height:auto;">
                </div>
              </div>
              <div class="item">
                <div class="testimonial-item style_2">
                    <img src="images/client/3.jpg" style="width:100%;height:auto;">
                </div>
              </div>
              <div class="item">
                <div class="testimonial-item style_2">
                    <img src="images/client/2.jpg" style="width:100%;height:auto;">
                </div>
              </div>
              <div class="item">
                <div class="testimonial-item style_2">
                    <img src="images/client/1.jpg" style="width:100%;height:auto;">
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-3" style="padding-top: 30px;">
            <div class="testimonial_style_2">
              <div class="item">
                <div class="testimonial-item style_2">
                    <img src="images/client/10.jpg" style="width:100%;height:auto;">
                </div>
              </div>
              <div class="item">
                <div class="testimonial-item style_2">
                    <img src="images/client/9.jpg" style="width:100%;height:auto;">
                </div>
              </div>
              <div class="item">
                <div class="testimonial-item style_2">
                    <img src="images/client/8.jpg" style="width:100%;height:auto;">
                </div>
              </div>
              <div class="item">
                <div class="testimonial-item style_2">
                    <img src="images/client/7.jpg" style="width:100%;height:auto;">
                </div>
              </div>
              <div class="item">
                <div class="testimonial-item style_2">
                    <img src="images/client/6.jpg" style="width:100%;height:auto;">
                </div>
              </div>
            </div>
          </div>
        </div>
        </div>
      </div>
    </div>
  </section>
  <!--Section:: Testimonial End -->
<?php include_once 'product-slider.php'; ?>
<?php include_once 'footer.php'; ?>
</div>
<!--End pagewrapper--> 

<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target=".main-header"><span class="icofont icofont-long-arrow-up"></span></div>
<script src="js/jquery.js"></script> 
<script src="js/jquery-ui-1.11.4/jquery-ui.js"></script> 
<script src="js/revolution.min.js"></script> 
<script src="js/rev-custom.js"></script>
<script src="js/all-jquery.js"></script> 
<script src="js/script.js"></script>
<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
<script>
  AOS.init();
</script>
</body>
</html>