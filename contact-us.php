<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>TRUWEIGH</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Stylesheets -->
<!-- <link rel="shortcut icon" type="image/png" href="images/favicon.png" /> -->
<link href="css/bootstrap.css" rel="stylesheet">
<!-- Font Icon -->
<link href="css/stroke-gap-icons.css" rel="stylesheet">
<link href="css/flaticon.css" rel="stylesheet">
<link href="css/font-awesome.min.css" rel="stylesheet">
<link href="css/icofont.css" rel="stylesheet">
<!-- Fancybox -->
<link href="css/jquery.fancybox.css" rel="stylesheet">
<!-- Revolution Slider -->
<link href="css/revolution-slider.css" rel="stylesheet">
<!-- Owl Carousel -->
<link href="css/owl.carousel.css" rel="stylesheet">
<!-- Main CSS -->
<link href="main-style.css" rel="stylesheet">
<!-- Responsive -->
<link href="css/responsive.css" rel="stylesheet">
<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--[if lt IE 9]><script src="js/respond.js"></script><![endif]-->
</head>
<style>
  .yellow-1
  {
    color:#FBCA00;
  }
  .featured-services--1
  {
    background:url("images/slider/contact-us.png");
    background-size:cover;
    background-position:center;
    background-repeat:no-repeat;
    width:auto;
    height:30vh;
  }
  .featured-services
  {
    background:linear-gradient(170deg,#787474b8,transparent),url(images/pitless_type.jpg);
    background-size:cover;
    background-position:center;
    background-repeat:no-repeat;
    background-attachment: fixed;
  }
  #text--2
  {
    position: relative;
    top:60px;
    left:50px;
  }
  #text--1
  {
    font-size: 60px;
    text-shadow: 3px 3px #e7ca1d;
  }
  #address-1{
    background: #0000008f;
    width: 330px;
    height: auto;
    padding: 6px;
    float: right;
  }
  #address-2{
    background: #0000008f;
    width: 330px;
    height: auto;
    padding: 6px;
    float: right;
    margin-top: 5px;
  }
  #address-3{
    background: #0000008f;
    width: 330px;
    height: auto;
    padding:6px;
    float: right;
    margin-top: 5px;
  }
  #icon-1
  {
    font-size:24px;
  }
  #icon-2
  {
    font-size: 24px;
  }
  #icon-3
  {
    font-size: 24px;
  }
  .google-maps {
        position: relative;
        padding-bottom: 75%; 
        height: 0;
        overflow: hidden;
    }
    .google-maps iframe {
        position: absolute;
        top: 0;
        left: 0;
        width: 100% !important;
        height: 100% !important;
    }
</style>
<body>
<div class="page-wrapper"> 
  <?php include_once 'header.php'; ?>
  <section class="featured-services--1">
      <div class="container">
          <div class="row">
            <div id="text--2">
              <h1 class="text-white" id="text--1" data-aos="fade-left" data-aos-easing="linear" data-aos-duration="1000" data-aos-once="true">Contacts Us</h1>
            </div>
          </div>
        </div>
  </section>
  <!--About Us TrueWay !-->
    <section class="featured-services">
      <div class="container" style='padding-top:30px;padding-bottom:0px;'>
      <div class="section-content">
        <div class="row">
          <div class="col-md-12" style='padding-top:0px;padding-bottom:14px;'>
            <h2 class=" text-theme-color text-center" >Contacts</h2> 
          </div>
        </div>
      </div>
        <div class="section-title">
          <div class="row" >
            <div class="col-md-6">
                <div id="address-1">
                  <address class="text-white">
                  <div id="icon-1">
                  <i class="fa fa-map-marker" aria-hidden="true"></i>
                  </div>
                     TRUWEIGH SYSTEMS INDIA PRIVATE LIMITED<br>
                    COSMAFAN Foundry Cluster Park - 1,<br>
                    Arasur, Coimbatore -641407<br>
                    TAMIL NADU, INDIA.<br>
                  </address>
                </div>
                <div id="address-2">
                  <address class="text-white">
                  <div id="icon-2">
                  <i class="fa fa-mobile" aria-hidden="true"></i>
                  </div>
                    +91 93622 77320
                  </address>
                </div>
                <div id="address-3">
                  <address class="text-white">
                    <div id="icon-3">
                      <i class="fa fa-envelope" aria-hidden="true"></i>
                    </div>
                    sales@truweigh.in 
                  </address>
                </div>
            </div>
            <div class="col-md-6 ">
              <div class="google-maps">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d62651.19570706649!2d77.01191751961082!3d11.061124073036053!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3ba8f8d3433f090f%3A0x1751c1763861e5b4!2sCosmafan%20Foundry%20Park%201%2C%20Arasur!5e0!3m2!1sen!2sin!4v1657614822247!5m2!1sen!2sin" width="800" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
              </div>
            </div>
          </div>
        </div>
    </section>
  <footer class="main-footer">
    <!--Copyright-->
   <div class="copyright"><a target="_blank" href="https://www.mediawonderz.com/">@2022 All Rights Reserved | Designed by Media Wonderz</a></div>
  </footer>
 
  <!--Section::Footer End --> 
</div>
<!--End pagewrapper--> 

<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target=".main-header"><span class="icofont icofont-long-arrow-up"></span></div>
<script src="js/jquery.js"></script> 
<script src="js/jquery-ui-1.11.4/jquery-ui.js"></script> 
<script src="js/revolution.min.js"></script> 
<script src="js/rev-custom.js"></script>
<script src="js/all-jquery.js"></script> 
<script src="js/script.js"></script>
<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
<script>
  AOS.init();
</script>
</body>
</html>