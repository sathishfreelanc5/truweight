<!DOCTYPE html>
<html>


<head>
<meta charset="utf-8">
<title>TRUWEIGH</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Stylesheets -->
<!-- <link rel="shortcut icon" type="image/png" href="images/favicon.png" /> -->
<link href="css/bootstrap.css" rel="stylesheet">
<!-- Font Icon -->
<link href="css/stroke-gap-icons.css" rel="stylesheet">
<link href="css/flaticon.css" rel="stylesheet">
<link href="css/font-awesome.min.css" rel="stylesheet">
<link href="css/icofont.css" rel="stylesheet">
<!-- Fancybox -->
<link href="css/jquery.fancybox.css" rel="stylesheet">
<!-- Revolution Slider -->
<link href="css/revolution-slider.css" rel="stylesheet">
<!-- Owl Carousel -->
<link href="css/owl.carousel.css" rel="stylesheet">
<!-- Main CSS -->
<link href="main-style.css" rel="stylesheet">
<!-- Responsive -->
<link href="css/responsive.css" rel="stylesheet">
<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--[if lt IE 9]><script src="js/respond.js"></script><![endif]-->
</head>
<style>
  .yellow-1
  {
    color:#FBCA00;
  }
  .featured-services--1
  {
    background:url("images/slider/product-banner.jpg");
    background-size:cover;
    background-position:center;
    background-repeat:no-repeat;
    width:auto;
    height:30vh;
  }
  .featured-services
  {
    background:linear-gradient(170deg,#787474b8,transparent),url(images/pitless_type.jpg);
    background-size:cover;
    background-position:center;
    background-repeat:no-repeat;
    background-attachment: fixed;
  }
  .blue-1
  {
      color:#0d599d;
  }
  .content-1
  {
    background:url("images/slider/content-banner.png");
    background-size:cover;
    background-position:center;
    background-repeat:no-repeat;
    height:25vh;
  }
  .fa-star-half
  {
      color:#FBCA00;
      font-size: 30px;
      margin-right: 5px;
  }
</style>
<body>
<div class="page-wrapper"> 
  <?php include_once 'header.php'; ?>
  <section class="featured-services--1">
      <div class="container">
          <div class="row">

          </div>
        </div>
  </section>
  <section class="featured-services--3">
      <div class="container" style='padding-top:50px;padding-bottom:50px;'>
      <div class="section-content">
        <div class="row">
          <div class="col-md-12" style='padding-top:0px;padding-bottom:14px;'>
            <h2 class=" text-theme-color text-center" >TANK / BIN<span class='blue-1'> WEIGHING SYSTEMS</span> </h2> 
          </div>
        </div>
      </div>
        <div class="section-title">
          <div class="row">
            <div class="col-md-12">
                  <div class="text-center"> 
                    <p>TRUWEIGH offers you a modular range of Bag filling/Weighing systems from which the basic models can be equipped with several options to obtain a complete solution for filling and weighing Bags. With the ACUTUZ® modular systems unstable, poorly filled and unstackable bags belong to the past. A correct assessment of the right equipment eliminates product loss, contamination and dust pollution during filling operation Careful consideration to filling quality and weighing accuracy have ensured the successful use of these filling machines.</p>
                 </div>
            </div>
          </div>
        </div>
    </section>
  <section class="testimonial style_2">
    <div class="container" style='padding-top:30px;padding-bottom:30px;'>
      <div class="section-content">
        <div class="row">
          <div class="col-md-6">
            <div class="testimonial_style_2">
              <div class="item">
                <div class="testimonial-item style_2">
                    <img src='images/tank.jpg' style='width:100%;height:auto;'>
                </div>
              </div>
              <div class="item">
                <div class="testimonial-item style_2">
                    <img src='images/tank.jpg' style='width:100%;height:auto;'>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class='row'>
                <div class='col-md-12' style='padding-bottom:60px;'>
                    <div >
                      <h2 class=" text-theme-color">PRODUCT<span class='blue-1'> KEY FEATURES</span> </h2> 
                    </div>
                </div>
                <div class='col-md-12' style='padding-bottom:20px;'>
                    <div class="testimonial-item style_2 ">
                        <p><i class="fa fa-star-half" aria-hidden="true"></i>Fully/Semi Automated Bag Weighers</p>
                        <p><i class="fa fa-star-half" aria-hidden="true"></i>High Accuracy</p>
                        <p><i class="fa fa-star-half" aria-hidden="true"></i>More productivity-100 Bags Per Hour</p>
                        <p><i class="fa fa-star-half" aria-hidden="true"></i>Less Man Power</p>
                        <p><i class="fa fa-star-half" aria-hidden="true"></i>Realtime Reporting System</p>
                        <p><i class="fa fa-star-half" aria-hidden="true"></i>Realtime Reporting System</p>
                    </div>
                </div>
            </div>
          </div>
        </div>
        </div>
      </div>
    </div>
  </section>
  <section class="content-1">
    <div class="container">
      <div class="section-content">
        <div class="row">
          <div class="col-md-12">
        
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--About Us TrueWay !-->
  <section class="call-to-action bg-black-pearl ">
    <div class="container"  style='padding-top:20px;padding-bottom:20px;'>
      <div class="section-content">
        <div class="row">
          <div class="col-md-12">
            <h2 class='text-theme-color text-center'>OUR PRODUCTS</h2> 
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="our_offer bg-light-grey ">
    <div class="container pb30" style='padding-top:50px;padding-bottom:50px;'>
        <div class="section-content">
          <div class="row">
            <div class="col-md-3 pn">
              <div class="item">
                  <div class="about-item inner-box">
                      <div class="image">
                          <img src="images/service/ser_1.jpg" alt="" class="img-responsive">
                          <span class="icon flaticon-barrel"></span>

                      </div>
                      <h3 class="mbn"><a href="#">Pit Type Weighbridge</a></h3>
                      <div class="view_more">
                        <a class="ptn" href="#">View details <span class="icofont icofont-arrow-right"></span></a>
                      </div>
                  </div>
              </div>
            </div>
            <div class="col-md-3 pn">
              <div class="item">
                  <div class="about-item inner-box">
                      <div class="image">
                          <img src="images/service/ser_2.jpg" alt="" class="img-responsive">
                          <span class="icon flaticon-harbor-crane"></span>
                          
                      </div>
                      <h3 class="mbn"><a href="#">Pitless Type Weighbridge</a></h3>
                      <div class="view_more">
                        <a class="ptn" href="#">View details <span class="icofont icofont-arrow-right"></span></a>
                      </div>
                  </div>
              </div>
            </div>
            <div class="col-md-3 pn">
              <div class="item">
                  <div class="about-item inner-box">
                      <div class="image">
                          <img src="images/service/ser_3.jpg" alt="" class="img-responsive">
                          <span class="icon flaticon-oil-platform"></span>
                          
                      </div>
                      <h3 class="mbn"><a href="#">Coil Weighbridge</a></h3>
                      <div class="view_more">
                        <a class="ptn" href="#">View details <span class="icofont icofont-arrow-right"></span></a>
                      </div>
                  </div>
              </div>
            </div>
            <div class="col-md-3 pn">
              <div class="item">
                  <div class="about-item inner-box">
                      <div class="image">
                          <img src="images/service/ser_4.jpg" alt="" class="img-responsive">
                          <span class="icon flaticon-tank-1"></span>
                      </div>
                      <h3 class="mbn"><a href="#">Mobile / Portable Weighbridge</a></h3>
                      <div class="view_more">
                        <a class="ptn" href="#">View details <span class="icofont icofont-arrow-right"></span></a>
                      </div>
                  </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-3 pn">
              <div class="item">
                  <div class="about-item inner-box">
                      <div class="image">
                          <img src="images/service/ser_5.jpg" alt="" class="img-responsive">
                          <span class="icon flaticon-barrel"></span>

                      </div>
                      <h3 class="mbn"><a href="#">CONCRETE WEIGHBRIDGE</a></h3>
                      <div class="view_more">
                        <a class="ptn" href="#">View details <span class="icofont icofont-arrow-right"></span></a>
                      </div>
                  </div>
              </div>
            </div>
            <div class="col-md-3 pn">
              <div class="item">
                  <div class="about-item inner-box">
                      <div class="image">
                          <img src="images/service/ser_6.jpg" alt="" class="img-responsive">
                          <span class="icon flaticon-harbor-crane"></span>
                          
                      </div>
                      <h3 class="mbn"><a href="#">Multi Deck</a></h3>
                      <div class="view_more">
                        <a class="ptn" href="#">View details <span class="icofont icofont-arrow-right"></span></a>
                      </div>
                  </div>
              </div>
            </div>
            <div class="col-md-3 pn">
              <div class="item">
                  <div class="about-item inner-box">
                      <div class="image">
                          <img src="images/service/ser_7.jpg" alt="" class="img-responsive">
                          <span class="icon flaticon-oil-platform"></span>
                          
                      </div>
                      <h3 class="mbn"><a href="#">Analog Load Cells</a></h3>
                      <div class="view_more">
                        <a class="ptn" href="#">View details <span class="icofont icofont-arrow-right"></span></a>
                      </div>
                  </div>
              </div>
            </div>
            <div class="col-md-3 pn">
              <div class="item">
                  <div class="about-item inner-box">
                      <div class="image">
                          <img src="images/service/ser_8.jpg" alt="" class="img-responsive">
                          <span class="icon flaticon-tank-1"></span>
                      </div>
                      <h3 class="mbn"><a href="#">Digital Load Cells</a></h3>
                      <div class="view_more">
                        <a class="ptn" href="#">View details <span class="icofont icofont-arrow-right"></span></a>
                      </div>
                  </div>
              </div>
            </div>
          </div>
       </div>
    </div>
  </section> 
<?php include_once 'footer.php'; ?>
</div>
<!--End pagewrapper--> 

<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target=".main-header"><span class="icofont icofont-long-arrow-up"></span></div>
<script src="js/jquery.js"></script> 
<script src="js/jquery-ui-1.11.4/jquery-ui.js"></script> 
<script src="js/revolution.min.js"></script> 
<script src="js/rev-custom.js"></script>
<script src="js/all-jquery.js"></script> 
<script src="js/script.js"></script>
</body>
</html>