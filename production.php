<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>TRUWEIGH | Production</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Stylesheets -->
<!-- <link rel="shortcut icon" type="image/png" href="images/favicon.png" /> -->
<link href="css/bootstrap.css" rel="stylesheet">
<!-- Font Icon -->
<link href="css/stroke-gap-icons.css" rel="stylesheet">
<link href="css/flaticon.css" rel="stylesheet">
<link href="css/font-awesome.min.css" rel="stylesheet">
<link href="css/icofont.css" rel="stylesheet">
<!-- Fancybox -->
<link href="css/jquery.fancybox.css" rel="stylesheet">
<!-- Revolution Slider -->
<link href="css/revolution-slider.css" rel="stylesheet">
<!-- Owl Carousel -->
<link href="css/owl.carousel.css" rel="stylesheet">
<!-- Main CSS -->
<link href="main-style.css" rel="stylesheet">
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Smooch+Sans:wght@100;200;300;400;500;600&display=swap" rel="stylesheet">
<!-- Responsive -->
<link href="css/responsive.css" rel="stylesheet">
<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--[if lt IE 9]><script src="js/respond.js"></script><![endif]-->
</head>
<style>
  .yellow-1
  {
    color:#FBCA00;
  }
  .featured-services--1
  {
    background:url("images/slider/production.png");
    background-size:cover;
    background-position:center;
    background-repeat:no-repeat;
    width:auto;
    height:30vh;
  }
  .featured-services
  {
    background:linear-gradient(170deg,#787474b8,transparent),url(images/pitless_type.jpg);
    background-size:cover;
    background-position:center;
    background-repeat:no-repeat;
    background-attachment: fixed;
  }
  #text--2
  {
    position: relative;
    top:60px;
    left:50px;
  }
  #text--1
  {
    font-size: 60px;
    text-shadow: 3px 3px #e7ca1d;
  }
</style>
<body>
<div class="page-wrapper"> 
  <?php include_once 'header.php'; ?>
  <section class="featured-services--1">
      <div class="container">
          <div class="row">
            <div id="text--2">
              <h1 class="text-white" id="text--1" data-aos="fade-left" data-aos-easing="linear" data-aos-duration="1000" data-aos-once="true">PRODUCTION & EXPORT</h1>
            </div>
          </div>
        </div>
  </section>

  <!--Section:: Testimonial-->
  <section class="testimonial style_2">
    <div class="container" style='padding-top:50px;padding-bottom:50px;'>
      <div class="section-content">
        <div class="row">
          <div class='col-md-12'>
              <div class='text-center' style='padding-top:50px;padding-bottom:20px;'>
                    <h2>PRODUCTION</h2>
              </div>
          </div>
          <div class="col-md-4">
            <div class="testimonial_style_2">
              <div class="item">
                <div class="testimonial-item style_2">
                    <h2 class="text-center">EXPORT DECK</h2>
                    <img src="images/export-deck.jpg" style="width:100%;height:auto;padding-top:20px;">
                </div>
              </div>
              <div class="item">
                <div class="testimonial-item style_2">
                    <h2 class="text-center">EXPORT DECK</h2>
                    <img src="images/export-deck.jpg" style="width:100%;height:auto;padding-top:20px;">
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="testimonial_style_2">
              <div class="item">
                <div class="testimonial-item style_2">
                    <h2 class="text-center">STUFFING VIDEO</h2>
                    <video width="320" height="240" controls>
                      <source src="video/export_stuffing_1.mp4" type="video/mp4">
                      <source src="video/export_stuffing_1.ogg" type="video/ogg">
                    </video>
                </div>
              </div>
              <div class="item">
                <div class="testimonial-item style_2">
                    <h2 class="text-center">STUFFING VIDEO</h2>
                    <video width="320" height="240" controls>
                      <source src="video/export_stuffing_1.mp4" type="video/mp4">
                      <source src="video/export_stuffing_1.ogg" type="video/ogg">
                    </video>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="testimonial_style_2">
              <div class="item">
                <div class="testimonial-item style_2">
                    <h2 class="text-center">STUFFING VIDEO</h2>
                    <video width="320" height="240" controls>
                      <source src="video/export_stuffing_2.mp4" type="video/mp4">
                      <source src="video/export_stuffing_2.ogg" type="video/ogg">
                    </video>
                </div>
              </div>
              <div class="item">
                <div class="testimonial-item style_2">
                    <h2 class="text-center">STUFFING VIDEO</h2>
                    <video width="320" height="240" controls>
                      <source src="video/export_stuffing_2.mp4" type="video/mp4">
                      <source src="video/export_stuffing_2.ogg" type="video/ogg">
                    </video>
                </div>
              </div>
            </div>
          </div>
        </div>
        </div>
      </div>
    </div>
  </section>
  <!--Section:: Testimonial End -->

  <section class="call-to-action bg-black-pearl">
    <div class="container"  style='padding-top:20px;padding-bottom:20px;'>
      <div class="section-content">
        <div class="row">
          <div class="col-md-12">
            <h2 class='text-theme-color text-center'>OUR PRODUCTS</h2> 
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="our_offer bg-light-grey">
    <div class="container pb30" style='padding-top:50px;padding-bottom:50px;'>
        <div class="section-content">
          <div class="row">
            <div class="col-md-3 pn">
              <div class="item">
                  <div class="about-item inner-box">
                      <div class="image">
                          <img src="images/service/ser_1.jpg" alt="" class="img-responsive">
                          <span class="icon flaticon-barrel"></span>

                      </div>
                      <h3 class="mbn"><a href="#">Pit Type Weighbridge</a></h3>
                      <div class="view_more">
                        <a class="ptn" href="#">View details <span class="icofont icofont-arrow-right"></span></a>
                      </div>
                  </div>
              </div>
            </div>
            <div class="col-md-3 pn">
              <div class="item">
                  <div class="about-item inner-box">
                      <div class="image">
                          <img src="images/service/ser_2.jpg" alt="" class="img-responsive">
                          <span class="icon flaticon-harbor-crane"></span>
                          
                      </div>
                      <h3 class="mbn"><a href="#">Pitless Type Weighbridge</a></h3>
                      <div class="view_more">
                        <a class="ptn" href="#">View details <span class="icofont icofont-arrow-right"></span></a>
                      </div>
                  </div>
              </div>
            </div>
            <div class="col-md-3 pn">
              <div class="item">
                  <div class="about-item inner-box">
                      <div class="image">
                          <img src="images/service/ser_3.jpg" alt="" class="img-responsive">
                          <span class="icon flaticon-oil-platform"></span>
                          
                      </div>
                      <h3 class="mbn"><a href="#">Coil Weighbridge</a></h3>
                      <div class="view_more">
                        <a class="ptn" href="#">View details <span class="icofont icofont-arrow-right"></span></a>
                      </div>
                  </div>
              </div>
            </div>
            <div class="col-md-3 pn">
              <div class="item">
                  <div class="about-item inner-box">
                      <div class="image">
                          <img src="images/service/ser_4.jpg" alt="" class="img-responsive">
                          <span class="icon flaticon-tank-1"></span>
                      </div>
                      <h3 class="mbn"><a href="#">Mobile / Portable Weighbridge</a></h3>
                      <div class="view_more">
                        <a class="ptn" href="#">View details <span class="icofont icofont-arrow-right"></span></a>
                      </div>
                  </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-3 pn">
              <div class="item">
                  <div class="about-item inner-box">
                      <div class="image">
                          <img src="images/service/ser_5.jpg" alt="" class="img-responsive">
                          <span class="icon flaticon-barrel"></span>

                      </div>
                      <h3 class="mbn"><a href="#">CONCRETE WEIGHBRIDGE</a></h3>
                      <div class="view_more">
                        <a class="ptn" href="#">View details <span class="icofont icofont-arrow-right"></span></a>
                      </div>
                  </div>
              </div>
            </div>
            <div class="col-md-3 pn">
              <div class="item">
                  <div class="about-item inner-box">
                      <div class="image">
                          <img src="images/service/ser_6.jpg" alt="" class="img-responsive">
                          <span class="icon flaticon-harbor-crane"></span>
                          
                      </div>
                      <h3 class="mbn"><a href="#">Multi Deck</a></h3>
                      <div class="view_more">
                        <a class="ptn" href="#">View details <span class="icofont icofont-arrow-right"></span></a>
                      </div>
                  </div>
              </div>
            </div>
            <div class="col-md-3 pn">
              <div class="item">
                  <div class="about-item inner-box">
                      <div class="image">
                          <img src="images/service/ser_7.jpg" alt="" class="img-responsive">
                          <span class="icon flaticon-oil-platform"></span>
                          
                      </div>
                      <h3 class="mbn"><a href="#">Analog Load Cells</a></h3>
                      <div class="view_more">
                        <a class="ptn" href="#">View details <span class="icofont icofont-arrow-right"></span></a>
                      </div>
                  </div>
              </div>
            </div>
            <div class="col-md-3 pn">
              <div class="item">
                  <div class="about-item inner-box">
                      <div class="image">
                          <img src="images/service/ser_8.jpg" alt="" class="img-responsive">
                          <span class="icon flaticon-tank-1"></span>
                      </div>
                      <h3 class="mbn"><a href="#">Digital Load Cells</a></h3>
                      <div class="view_more">
                        <a class="ptn" href="#">View details <span class="icofont icofont-arrow-right"></span></a>
                      </div>
                  </div>
              </div>
            </div>
          </div>
       </div>
    </div>
  </section> 
<?php include_once 'footer.php'; ?>
</div>
<!--End pagewrapper--> 

<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target=".main-header"><span class="icofont icofont-long-arrow-up"></span></div>
<script src="js/jquery.js"></script> 
<script src="js/jquery-ui-1.11.4/jquery-ui.js"></script> 
<script src="js/revolution.min.js"></script> 
<script src="js/rev-custom.js"></script>
<script src="js/all-jquery.js"></script> 
<script src="js/script.js"></script>
<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
<script>
  AOS.init();
</script>
</body>
</html>