 <section id="head--1" >
  <div class="container">
      <div class="row">
          <div class="col-md-6">
              <div id="mob-2">
                  <p class="text-white" id="mob-1">
                    <i class="fa fa-phone" aria-hidden="true"></i>
                    +91 93622 77320
                  </p>
              </div>
          </div>
          <div class="col-md-5">
            <div id="mail-2">
               <p class="text-white" id="mail-1">
                <i class="fa fa-envelope-o" aria-hidden="true"></i>
                sales@truweigh.in
              </p>
            </div>
          </div>
          <div class="col-md-1" id="col--1">&nbsp;</div>
      </div> 
  <div>
</section>
  <!-- Main Header / Style One-->
  <header class="main-header header-style-two">    
    <!--Header-Main Box-->
    <div class="header-mainbox style_3">
      <div class="container ptn pbn">
        <div class="clearfix">
          <div class="logo-box">
            <div class="logo"> 
              <a href="index.php">
                  <img src="images/logo.jpg" alt="logo" class="logo1">
               </a> 
            </div>
          </div>
          <div class="outer-box clearfix"> 
            <!-- Main Menu -->
            <nav class="main-menu logo-outer navbar navbar-light bg-light">
              <div class="navbar-header"> 
                <!-- Toggle Button -->
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
              </div>
              <div class="navbar-collapse collapse clearfix">
                <ul class="navigation clearfix">
                  <li class="current "> <a href="index.php">HOME</a></li>
                  <li class="dropdown"> <a href="#">ABOUT US</a>
                    <ul class='list-2'>
                      <!-- <li><a href="about.php">About</a></li> -->
                      <li><a href="industries.php">Industries</a></li>
                      <li><a href="gallery.php">Gallery</a></li>
                      <!-- <li><a href="client.php">Clients</a></li> -->
                    </ul>
                  </li>
                  <li class="dropdown"> <a href="#">PRODUCTS</a>
                    <ul class='list-1'>
                        <li class="dropdown"> 
                           <a href="#">Weighbridge</a>
                           <ul class='list-1'>
                                <li><a href="pit-type-weigh.php" >Pit Type Weighbridge</a></li>
                                <li><a href="pitless-type.php">Pitless Type Weighbridge</a></li>
                                <li><a href="mobile_calibaration.php">Mobile Calibration Truck</a></li>
                                <li><a href="concrete.php">Concrete Weighbridge</a></li>
                           </ul>
                        </li>
                        <li class="dropdown"> 
                           <a href="#">Electronics</a>
                           <ul class='list-1'>
                              <li><a href="digital_load_cell.php">Digital Load Cells</a></li>
                              <li><a href="analog_load_cell.php">Analog Load Cells</a></li>
                              <li><a href="accessories.php">accessories</a>	</li>
                           </ul>
                        </li>
<!--                     
                      <li><a href="mobile_weighbridge.php">Mobile / Portable Weighbridge</a></li>
                      <li><a href="truweigh_indicator.php">Truweigh Indicator</a></li>
                       -->
                    </ul>
                  </li>
                  <li><a href="service.php">SERVICES</a></li>
                 
                  <li><a href="contact-us.php">CONTACT US</a></li>   
                  
                </ul>
              </div>
            </nav>
            <!-- Main Menu End--> 
          </div>
        </div>
      </div>
    </div>
    <!--Header Main Box End--> 
  </header>
  <!--End Main Header --> 
  