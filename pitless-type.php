<!DOCTYPE html>
<html>


<head>
<meta charset="utf-8">
<title>TRUWEIGH</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Stylesheets -->
<!-- <link rel="shortcut icon" type="image/png" href="images/favicon.png" /> -->
<link href="css/bootstrap.css" rel="stylesheet">
<!-- Font Icon -->
<link href="css/stroke-gap-icons.css" rel="stylesheet">
<link href="css/flaticon.css" rel="stylesheet">
<link href="css/font-awesome.min.css" rel="stylesheet">
<link href="css/icofont.css" rel="stylesheet">
<!-- Fancybox -->
<link href="css/jquery.fancybox.css" rel="stylesheet">
<!-- Revolution Slider -->
<link href="css/revolution-slider.css" rel="stylesheet">
<!-- Owl Carousel -->
<link href="css/owl.carousel.css" rel="stylesheet">
<!-- Main CSS -->
<link href="main-style.css" rel="stylesheet">
<!-- Responsive -->
<link href="css/responsive.css" rel="stylesheet">
<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--[if lt IE 9]><script src="js/respond.js"></script><![endif]-->
</head>
<style>
  .yellow-1
  {
    color:#FBCA00;
  }
  .featured-services--1
  {
    background:url("images/slider/product-banner.jpg");
    background-size:cover;
    background-position:center;
    background-repeat:no-repeat;
    width:auto;
    height:30vh;
  }
  .featured-services
  {
    background:linear-gradient(170deg,#787474b8,transparent),url(images/pitless_type.jpg);
    background-size:cover;
    background-position:center;
    background-repeat:no-repeat;
    background-attachment: fixed;
  }
  .blue-1
  {
      color:#0d599d;
  }
  .content-1
  {
    background:url("images/slider/content-banner.png");
    background-size:cover;
    background-position:center;
    background-repeat:no-repeat;
    height:25vh;
  }
  
</style>
<body>
<div class="page-wrapper"> 
  <?php include_once 'header.php'; ?>
  <section class="featured-services--1">
      <div class="container">
          <div class="row">

          </div>
        </div>
  </section>
  <section class="featured-services--3">
      <div class="container" style='padding-top:50px;padding-bottom:50px;'>
      <div class="section-content">
        <div class="row">
          <div class="col-md-12" style='padding-top:0px;padding-bottom:14px;'>
            <h2 class="text-center" >PITLESS TYPE WEIGHBRIDGE</h2> 
          </div>
        </div>
      </div>
        <div class="section-title">
          <div class="row">
            <div class="col-md-12">
                  <div class="text-center"> 
                    <p>We offer Pitless weighbridges to industrial purposes where space is not a major constraint. These Pitless type weighbridges are useful in areas where hazardous gases are present in the atmosphere and in places where soil is rocky. Pitless type weighbridges also offer very good services in water logging areas. </p>
                 </div>
            </div>
          </div>
        </div>
    </section>
  <section class="testimonial style_2">
    <div class="container" style='padding-top:30px;padding-bottom:30px;'>
      <div class="section-content">
        <div class="row">
          <div class="col-md-6">
            <div class="testimonial_style_2">
              <div class="item">
                <div class="testimonial-item style_2">
                    <img src='images/pitless_type.jpg' style='width:100%;height:auto;'>
                </div>
              </div>
              <div class="item">
                <div class="testimonial-item style_2">
                    <img src='images/pitless_type.jpg' style='width:100%;height:auto;'>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class='row'>
                <div class='col-md-12' style='padding-bottom:20px;'>
                    <div >
                      <h2 >PRODUCT ADVANTAGES</span> </h2> 
                    </div>
                </div>
                <div class='col-md-12' style='padding-bottom:20px;'>
                    <div class="testimonial-item style_2 ">
                        <p><i class="fa fa-star-half" aria-hidden="true"></i> Suitable for industrial weigh bridges</p>
                        <p><i class="fa fa-star-half" aria-hidden="true"></i> Suitable for places where the excavation work for foundation is difficult</p>
                        <p><i class="fa fa-star-half" aria-hidden="true"></i> Construction of civil foundation is cheaper</p>
                        <p><i class="fa fa-star-half" aria-hidden="true"></i> No water logging during raining season</p>
                        <p><i class="fa fa-star-half" aria-hidden="true"></i>PIT maintenance is eliminated</p>
                        <p><i class="fa fa-star-half" aria-hidden="true"></i> Easy maintenance since all parts are accessible above the ground level</p>
                        <p><i class="fa fa-star-half" aria-hidden="true"></i> Wide range of capacities 5mt. to 150 mt.</p>
                        <p><i class="fa fa-star-half" aria-hidden="true"></i> Platform sizes 2 X 2m to 20 X 3m</p>
                    </div>
                </div>
            </div>
          </div>
        </div>
        </div>
      </div>
    </div>
  </section>
  <section class="content-1">
    <div class="container">
      <div class="section-content">
        <div class="row">
          <div class="col-md-12">
        
          </div>
        </div>
      </div>
    </div>
  </section>
<?php include_once 'product-slider.php'; ?>
<?php include_once 'footer.php'; ?>
</div>
<!--End pagewrapper--> 

<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target=".main-header"><span class="icofont icofont-long-arrow-up"></span></div>
<script src="js/jquery.js"></script> 
<script src="js/jquery-ui-1.11.4/jquery-ui.js"></script> 
<script src="js/revolution.min.js"></script> 
<script src="js/rev-custom.js"></script>
<script src="js/all-jquery.js"></script> 
<script src="js/script.js"></script>
</body>


</html>