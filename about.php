<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>TRUWEIGH</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Stylesheets -->
<!-- <link rel="shortcut icon" type="image/png" href="images/favicon.png" /> -->
<link href="css/bootstrap.css" rel="stylesheet">
<!-- Font Icon -->
<link href="css/stroke-gap-icons.css" rel="stylesheet">
<link href="css/flaticon.css" rel="stylesheet">
<link href="css/font-awesome.min.css" rel="stylesheet">
<link href="css/icofont.css" rel="stylesheet">
<!-- Fancybox -->
<link href="css/jquery.fancybox.css" rel="stylesheet">
<!-- Revolution Slider -->
<link href="css/revolution-slider.css" rel="stylesheet">
<!-- Owl Carousel -->
<link href="css/owl.carousel.css" rel="stylesheet">
<!-- Main CSS -->
<link href="main-style.css" rel="stylesheet">
<!-- Responsive -->
<link href="css/responsive.css" rel="stylesheet">
<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--[if lt IE 9]><script src="js/respond.js"></script><![endif]-->
</head>
<style>
  .yellow-1
  {
    color:#FBCA00;
  }
  .featured-services--1
  {
    background:url("images/slider/about-banner.jpg");
    background-size:cover;
    background-position:center;
    background-repeat:no-repeat;
    width:auto;
    height:30vh;
  }
  .featured-services
  {
    background:linear-gradient(170deg,#787474b8,transparent),url(images/pitless_type.jpg);
    background-size:cover;
    background-position:center;
    background-repeat:no-repeat;
    background-attachment: fixed;
  }
  #text--2
  {
    position: relative;
    top:60px;
    left:50px;
  }
  #text--1
  {
    font-size: 60px;
    text-shadow: 3px 3px #e7ca1d;
  }
</style>
<body>
<div class="page-wrapper"> 
  <?php include_once 'header.php'; ?>
  <section class="featured-services--1">
      <div class="container">
          <div class="row">
            <div id="text--2">
              <h1 class="text-white" id="text--1" data-aos="fade-left" data-aos-easing="linear" data-aos-duration="1000" data-aos-once="true">About Us</h1>
            </div>
          </div>
        </div>
  </section>
  <!--About Us TrueWay !-->
    <section class="featured-services">
      <div class="container" style='padding-top:30px;padding-bottom:0px;'>
      <div class="section-content">
        <div class="row">
          <div class="col-md-12" style='padding-top:0px;padding-bottom:14px;'>
            <h2 class="text-center" >WELCOME TO  <span>TRUWEIGH SYSTEMS</span> </h2> 
          </div>
        </div>
      </div>
        <div class="section-title">
          <div class="row">
            <div class="col-md-12">
                  <div class="text-center"> 
                    <p style='color:white;'>TRUWEIGH® is a well established Brand in the field of Electronic Weighbridges under the Company named “TRUWEIGH SYSTEMS INDIA PVT. LTD.” who is one of the leading Manufacturers, Suppliers, Service Providers & Calibrators of all types of Electronic Weighbridges, Weighing Systems and Weighing Automation Solutions. With the vast experience in the field of Weighing for more than 50 years cumulatively holded by the founders, TRUWEIGH® becomes one of the TRUSTED supplier in the field of Weighbridges for more than a Decade. TRUWEIGH® believes in “Quality” and it is being followed very strictly in every part of actions right from DESIGN to AFTER SALES SERVICE at affordable cost to deliver the PRODUCTS & SERVICES to TRUWEIGH® ’s valuable customers. </p>
                    <p style='color:white;'>This belief takes the company to the next levels progressively year after year & today TRUWEIGH® is having more than 800 satisfied installations spread over the Country and Overseas. Our installations includes from Public Weighbridges to leading MNC Companies in INDIA & OVERSEAS, State & Central Government Organizations including SAIL, Defence, SEA PORTS etc.,</p>
                 </div>
            </div>
          </div>
        </div>
    </section>

  <!--Section:: Testimonial-->
  <section class="testimonial style_2">
    <div class="container" style='padding-top:50px;padding-bottom:50px;'>
      <div class="section-content">
        <div class="row">
          <div class='col-md-12'>
              <div class='text-center' style='padding-top:10px;padding-bottom:20px;'>
                    <h2>OUR COMMITMENTS INCLUDE </h2>
              </div>
          </div>
          <div class="col-md-6">
            <div class="testimonial_style_2">
              <div class="item">
                <div class="testimonial-item style_2">
                    <p><i class="fa fa-star-half" aria-hidden="true"></i> Use of latest cutting edge technology in weighing industry</p>
                    <p><i class="fa fa-star-half" aria-hidden="true"></i> Turnkey Solutions for Projects</p>
                    <p><i class="fa fa-star-half" aria-hidden="true"></i> Providing Total Solutions</p>
                </div>
              </div>
              <div class="item">
                <div class="testimonial-item style_2">
                      <p><i class="fa fa-star-half" aria-hidden="true"></i>All types of fully electronic weigh bridges & systems with Flintec Digital load cell which adopts the latest cutting edge technology.  All types of fully electronic weigh bridges & systems with Flintec Digital load cell which adopts the latest cutting edge technology.</p>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="testimonial_style_2">
              <div class="item">
                <div class="testimonial-item style_2">
                  <p><i class="fa fa-star-half" aria-hidden="true"></i>All types of fully electronic weigh bridges & systems with Flintec Digital load cell which adopts the latest cutting edge technology.  All types of fully electronic weigh bridges & systems with Flintec Digital load cell which adopts the latest cutting edge technology.</p>
                </div>
              </div>
              <div class="item">
                <div class="testimonial-item style_2">
                    <p><i class="fa fa-star-half" aria-hidden="true"></i>All types of fully electronic weigh bridges & systems with Flintec Digital load cell which adopts the latest cutting edge technology.  All types of fully electronic weigh bridges & systems with Flintec Digital load cell which adopts the latest cutting edge technology.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        </div>
      </div>
    </div>
  </section>
  <!--Section:: Testimonial End -->

<?php include_once 'product-slider.php'; ?>
<?php include_once 'footer.php'; ?>
</div>
<!--End pagewrapper--> 

<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target=".main-header"><span class="icofont icofont-long-arrow-up"></span></div>
<script src="js/jquery.js"></script> 
<script src="js/jquery-ui-1.11.4/jquery-ui.js"></script> 
<script src="js/revolution.min.js"></script> 
<script src="js/rev-custom.js"></script>
<script src="js/all-jquery.js"></script> 
<script src="js/script.js"></script>
<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
<script>
  AOS.init();
</script>
</body>


</html>