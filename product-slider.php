<section class="call-to-action bg-black-pearl">
    <div class="container"  style='padding-top:20px;padding-bottom:20px;'>
      <div class="section-content">
        <div class="row">
          <div class="col-md-12">
            <h2 class='text-theme-color text-center'>OUR PRODUCTS</h2> 
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="our_offer bg-light-grey">
    <div class="container pb30" style='padding-top:50px;padding-bottom:50px;'>
        <div class="section-content"> 
          <div class="row">
            <div class="col-md-3 pn">
              <div class="item">
                  <div class="about-item inner-box">
                      <div class="image">
                          <img src="images/service/ser_1.jpg" alt="" class="img-responsive">
                          <span class="icon flaticon-barrel"></span>

                      </div>
                      <h3 class="mbn"><a href="#">Pit Type Weighbridge</a></h3>
                      <div class="view_more">
                        <a class="ptn" href="#">View details <span class="icofont icofont-arrow-right"></span></a>
                      </div>
                  </div>
              </div>
            </div>
            <div class="col-md-3 pn">
              <div class="item">
                  <div class="about-item inner-box">
                      <div class="image">
                          <img src="images/service/ser_2.jpg" alt="" class="img-responsive">
                          <span class="icon flaticon-harbor-crane"></span>
                          
                      </div>
                      <h3 class="mbn"><a href="#">Pitless Type Weighbridge</a></h3>
                      <div class="view_more">
                        <a class="ptn" href="#">View details <span class="icofont icofont-arrow-right"></span></a>
                      </div>
                  </div>
              </div>
            </div>
            <div class="col-md-3 pn">
              <div class="item">
                  <div class="about-item inner-box">
                      <div class="image">
                          <img src="images/service/ser_3.jpg" alt="" class="img-responsive">
                          <span class="icon flaticon-oil-platform"></span>
                          
                      </div>
                      <h3 class="mbn"><a href="#">Coil Weighbridge</a></h3>
                      <div class="view_more">
                        <a class="ptn" href="#">View details <span class="icofont icofont-arrow-right"></span></a>
                      </div>
                  </div>
              </div>
            </div>
            <div class="col-md-3 pn">
              <div class="item">
                  <div class="about-item inner-box">
                      <div class="image">
                          <img src="images/service/ser_4.jpg" alt="" class="img-responsive">
                          <span class="icon flaticon-tank-1"></span>
                      </div>
                      <h3 class="mbn"><a href="#">Mobile / Portable Weighbridge</a></h3>
                      <div class="view_more">
                        <a class="ptn" href="#">View details <span class="icofont icofont-arrow-right"></span></a>
                      </div>
                  </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-3 pn">
              <div class="item">
                  <div class="about-item inner-box">
                      <div class="image">
                          <img src="images/service/ser_5.jpg" alt="" class="img-responsive">
                          <span class="icon flaticon-barrel"></span>

                      </div>
                      <h3 class="mbn"><a href="#">CONCRETE WEIGHBRIDGE</a></h3>
                      <div class="view_more">
                        <a class="ptn" href="#">View details <span class="icofont icofont-arrow-right"></span></a>
                      </div>
                  </div>
              </div>
            </div>
            <!-- <div class="col-md-3 pn">
              <div class="item">
                  <div class="about-item inner-box">
                      <div class="image">
                          <img src="images/service/ser_6.jpg" alt="" class="img-responsive">
                          <span class="icon flaticon-harbor-crane"></span>
                          
                      </div>
                      <h3 class="mbn"><a href="#">Multi Deck</a></h3>
                      <div class="view_more">
                        <a class="ptn" href="#">View details <span class="icofont icofont-arrow-right"></span></a>
                      </div>
                  </div>
              </div>
            </div> -->
            <div class="col-md-3 pn">
              <div class="item">
                  <div class="about-item inner-box">
                      <div class="image">
                          <img src="images/service/ser_7.jpg" alt="" class="img-responsive">
                          <span class="icon flaticon-oil-platform"></span>
                          
                      </div>
                      <h3 class="mbn"><a href="#">Analog Load Cells</a></h3>
                      <div class="view_more">
                        <a class="ptn" href="#">View details <span class="icofont icofont-arrow-right"></span></a>
                      </div>
                  </div>
              </div>
            </div>
            <div class="col-md-3 pn">
              <div class="item">
                  <div class="about-item inner-box">
                      <div class="image">
                          <img src="images/service/ser_8.jpg" alt="" class="img-responsive">
                          <span class="icon flaticon-tank-1"></span>
                      </div>
                      <h3 class="mbn"><a href="#">Digital Load Cells</a></h3>
                      <div class="view_more">
                        <a class="ptn" href="#">View details <span class="icofont icofont-arrow-right"></span></a>
                      </div>
                  </div>
              </div>
            </div>
          </div>
       </div>
    </div>
  </section> 